# Use a base image with Ubuntu
FROM ubuntu:latest

# Update package lists and install dependencies
RUN apt-get update && \
    apt-get install -y \
    tshark \
    && rm -rf /var/lib/apt/lists/*

# Create a non-root user
RUN useradd -ms /bin/bash appuser

# Set the working directory inside the container
WORKDIR /app

# Copy your shell script into the container
COPY http_traffic.sh /app

# Make your script executable
RUN chmod +x /app/http_traffic.sh

# Set the ownership of the script to the non-root user
RUN chown appuser:appuser /app/http_traffic.sh

# Specify the non-root user to run the container
USER appuser

# Specify the entry point for your script
ENTRYPOINT ["/app/http_traffic.sh"]
