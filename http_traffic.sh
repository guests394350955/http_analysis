#!/bin/bash

# Check if the user provided a pcap file argument
if [ -z "$1" ]; then
    echo "Usage: $0 <pcap_file>"
    exit 1
fi

# Run tshark command and save the output to the array
mapfile -t output_array < <(tshark -r "$1" -2 -T fields -e frame.number -e http.request_in -e http.response_in -Y http)

# Initialize counter for HTTP request-response pairs
http_flows=0

# Iterate over the array and count HTTP request-response pairs
for line in "${output_array[@]}"; do
    IFS=$'\t' read -r -a fields <<< "$line"
    if [[ -n "${fields[1]}" || -n "${fields[2]}" ]]; then
        http_flows=$((http_flows + 1))
    fi
done

echo "http traffic flows:" $http_flows

# Summing Up the total http data bytes to calculate the total http traffic
echo "http traffic bytes:" $(tshark -r "$1" -Y "http" -T fields -e data.len | awk '{ total += $1 } END { print total }')

# Determing the most visited hostname based on the http request host
echo "top http hostname:" $(tshark -r "$1" -Y http.request -T fields -e http.host | sort | uniq -c | sort -nr | head -n1 | awk '{print $2}' )

